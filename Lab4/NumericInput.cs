﻿using System;
using System.Windows.Forms;

namespace Lab4
{
    public partial class NumericInput : UserControl
    {
        public NumericInput()
        {
            InitializeComponent();
        }

        public void RegisterUpdateHandler(EventHandler handler)
        {
            valuePicker.ValueChanged += handler;
        }

        public double GetValue()
        {
            return (double) this.valuePicker.Value;
        }

        public void SetValue(double value)
        {
            this.valuePicker.Value = (decimal) value;
        }

        public void SetMinValue(double value)
        {
            this.valuePicker.Minimum = (decimal) value;
        }

        public void SetMaxValue(double value)
        {
            this.valuePicker.Maximum = (decimal) value;
        }

        public void SetLabel(string label)
        {
            this.label.Text = label;
        }

    }
}
