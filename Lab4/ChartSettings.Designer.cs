﻿namespace Lab4
{
    partial class ChartSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.minLabel = new System.Windows.Forms.Label();
            this.maxLabel = new System.Windows.Forms.Label();
            this.minValue = new System.Windows.Forms.TextBox();
            this.maxValue = new System.Windows.Forms.TextBox();
            this.lowerBound = new Lab4.NumericInput();
            this.upperBound = new Lab4.NumericInput();
            this.step = new Lab4.NumericInput();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 83F));
            this.tableLayoutPanel1.Controls.Add(this.lowerBound, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.upperBound, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.step, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.minLabel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.maxLabel, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.minValue, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.maxValue, 1, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(150, 150);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // minLabel
            // 
            this.minLabel.AutoSize = true;
            this.minLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.minLabel.Location = new System.Drawing.Point(3, 108);
            this.minLabel.Name = "minLabel";
            this.minLabel.Size = new System.Drawing.Size(61, 20);
            this.minLabel.TabIndex = 3;
            this.minLabel.Text = "min";
            // 
            // maxLabel
            // 
            this.maxLabel.AutoSize = true;
            this.maxLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maxLabel.Location = new System.Drawing.Point(3, 128);
            this.maxLabel.Name = "maxLabel";
            this.maxLabel.Size = new System.Drawing.Size(61, 22);
            this.maxLabel.TabIndex = 4;
            this.maxLabel.Text = "max";
            // 
            // minValue
            // 
            this.minValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.minValue.Location = new System.Drawing.Point(70, 111);
            this.minValue.Name = "minValue";
            this.minValue.Size = new System.Drawing.Size(77, 22);
            this.minValue.TabIndex = 5;
            // 
            // maxValue
            // 
            this.maxValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maxValue.Location = new System.Drawing.Point(70, 131);
            this.maxValue.Name = "maxValue";
            this.maxValue.Size = new System.Drawing.Size(77, 22);
            this.maxValue.TabIndex = 6;
            // 
            // lowerBound
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.lowerBound, 2);
            this.lowerBound.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lowerBound.Location = new System.Drawing.Point(3, 3);
            this.lowerBound.Name = "lowerBound";
            this.lowerBound.Size = new System.Drawing.Size(144, 30);
            this.lowerBound.TabIndex = 0;
            // 
            // upperBound
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.upperBound, 2);
            this.upperBound.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upperBound.Location = new System.Drawing.Point(3, 39);
            this.upperBound.Name = "upperBound";
            this.upperBound.Size = new System.Drawing.Size(144, 30);
            this.upperBound.TabIndex = 1;
            // 
            // step
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.step, 2);
            this.step.Dock = System.Windows.Forms.DockStyle.Fill;
            this.step.Location = new System.Drawing.Point(3, 75);
            this.step.Name = "step";
            this.step.Size = new System.Drawing.Size(144, 30);
            this.step.TabIndex = 2;
            // 
            // ChartSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ChartSettings";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private NumericInput lowerBound;
        private NumericInput upperBound;
        private NumericInput step;
        private System.Windows.Forms.Label minLabel;
        private System.Windows.Forms.Label maxLabel;
        private System.Windows.Forms.TextBox minValue;
        private System.Windows.Forms.TextBox maxValue;
    }
}
