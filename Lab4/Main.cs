﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab4
{
    public partial class Main : Form
    {

        private static TabulatedFunction taskAFun = new TabulatedFunction(
            "sin(x) / (2 + cos(x))", x =>
            {
                var sin = Math.Sin(x);
                var cos = Math.Cos(x);

                return sin / (2.0 + cos);
            }
        );

        private static TabulatedFunction taskBFunX = new TabulatedFunction(
            "cos^4(t)", t => Math.Pow(Math.Cos(t), 4.0)
        );

        private static TabulatedFunction taskBFunY = new TabulatedFunction(
            "sin^4(t)", t => Math.Pow(Math.Sin(t), 4.0)
        );



        public Main()
        {
            InitializeComponent();

            IEnumerable<TabulatedFunction> tabulatedFunctions = Enumerable
                .Empty<TabulatedFunction>()
                .Append(taskAFun)
                .Append(taskBFunX)
                .Append(taskBFunY)
                .Append(taskBFunX.Compose(taskBFunY))
                .Append(taskBFunY.Compose(taskBFunX));

            tabControl.TabPages.Clear();

            tabControl.TabPages.AddRange(
                tabulatedFunctions
                    .Select(fn =>
                    {
                        FunctionPlotter plotter = new FunctionPlotter();

                        TabPage page = new TabPage(fn.ReadableName);

                        page.Controls.Add(plotter);
                        page.Controls[0].Dock = DockStyle.Fill;

                        plotter.SetFunction(fn);

                        return page;
                    })
                    .ToArray()
            );
        }
    }
}
