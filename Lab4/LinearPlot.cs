﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4
{
    public partial class LinearPlot : UserControl
    {
        private static class LinearPlotSettings
        {
            public static Color PlotBackgroundColor = Color.White;
            public static Brush Brush = new SolidBrush(Color.Black);
            public static Pen PlotPen = new Pen(Color.Red, 1.5F);
            public static Pen AxesPen = new Pen(Color.FromArgb(127, 0, 0, 0));
            public static Font Font = new Font(FontFamily.GenericSansSerif, 7.5F);
            public static int LabelSize = 50;
            public static int MarginLeft = 50;
            public static int MarginTop = 30;
            public static int MarginBottom = 50;
            public static int MarginRight = 10;
            public static int FontWidth = 30;
        }


        public readonly List<Point> Points = new List<Point>();
        public string FunctionName;


        private void drawFunctionName(Graphics gr)
        {
            if (FunctionName != null)
            {
                float verticalAlign = LinearPlotSettings.FontWidth / 3.0F;

                gr.FillRectangle(
                    LinearPlotSettings.PlotPen.Brush,
                    new RectangleF(LinearPlotSettings.MarginLeft - 20, 
                                   verticalAlign + LinearPlotSettings.Font.GetHeight() / 2.0F - LinearPlotSettings.PlotPen.Width / 2.0F, 
                                   10.0F, 
                                   LinearPlotSettings.PlotPen.Width)
                );
                gr.DrawString(
                    FunctionName, 
                    LinearPlotSettings.Font, 
                    LinearPlotSettings.Brush, 
                    new PointF(LinearPlotSettings.MarginLeft, verticalAlign)
                );
            }
        }
        private void drawAxes(Graphics gr, double minX, double maxX, double minY, double maxY)
        {
            float innerWidth = gr.ClipBounds.Width - LinearPlotSettings.MarginLeft - LinearPlotSettings.MarginRight;
            float innerHeight = gr.ClipBounds.Height - LinearPlotSettings.MarginTop - LinearPlotSettings.MarginBottom;

            int xLabelsCount = (int) (innerWidth / LinearPlotSettings.LabelSize);
            int yLabelsCount = (int) (innerHeight / LinearPlotSettings.LabelSize);

            float xStep = innerWidth / xLabelsCount;
            float yStep = innerHeight / yLabelsCount;

            double funcXStep = (maxX - minX) / xLabelsCount;
            double funcYStep = (maxY - minY) / yLabelsCount;

            for (int i = 0; i <= xLabelsCount; ++i)
            {
                float x = gr.ClipBounds.X + LinearPlotSettings.MarginLeft + i * xStep;
                float bottom = gr.ClipBounds.Y + LinearPlotSettings.MarginTop + innerHeight;
                gr.DrawLine(LinearPlotSettings.AxesPen, 
                            x, 
                            gr.ClipBounds.Y + LinearPlotSettings.MarginTop, 
                            x,
                            bottom);
                double funcX = minX + i * funcXStep;

                gr.DrawString(
                    String.Format("{0:0.00}", funcX),
                    LinearPlotSettings.Font, 
                    LinearPlotSettings.Brush, 
                    new PointF(x - LinearPlotSettings.FontWidth / 2.0F, bottom)
                );
            }
            for (int i = 0; i < yLabelsCount; ++i)
            {
                float y = gr.ClipBounds.Y + LinearPlotSettings.MarginTop + i * yStep;
                float x = gr.ClipBounds.X + LinearPlotSettings.MarginLeft;

                gr.DrawLine(LinearPlotSettings.AxesPen,
                            x,
                            y,
                            x + innerWidth,
                            y);

                double funcY = maxY - i * funcYStep;

                gr.DrawString(
                    String.Format("{0:0.00}", funcY),
                    LinearPlotSettings.Font,
                    LinearPlotSettings.Brush,
                    new PointF(x - LinearPlotSettings.FontWidth, y)
                );
            }

        }

        private void drawPlot(Graphics gr, double minX, double maxX, double minY, double maxY)
        {
            float leftX = gr.ClipBounds.X;
            float rightX = leftX + gr.ClipBounds.Width;
            double scaleX = (maxX - minX) / gr.ClipBounds.Width;

            float upperY = gr.ClipBounds.Location.Y;
            float lowerY = upperY + gr.ClipBounds.Height;
            double scaleY = (maxY - minY) / gr.ClipBounds.Height;

            var scaledPoints = Points.Select(point => new PointF(
                (float) ((point.X - minX) / scaleX + leftX),
                (float) (lowerY - (point.Y - minY) / scaleY)
            )).ToArray();

            for (int i = 0; i < scaledPoints.Length - 1; ++i)
            {
                gr.DrawLine(LinearPlotSettings.PlotPen, scaledPoints[i], scaledPoints[i + 1]);
            }
        }

        private void draw(Graphics graphics)
        {
            graphics.Clear(LinearPlotSettings.PlotBackgroundColor);

            if (Points.Count() > 0)
            {
                double minX = Points.Select(p => p.X).Min();
                double maxX = Points.Select(p => p.X).Max();
                double minY = Points.Select(p => p.Y).Min();
                double maxY = Points.Select(p => p.Y).Max();

                drawFunctionName(graphics);
                drawAxes(graphics, minX, maxX, minY, maxY);

                graphics.Clip = new Region(new RectangleF(
                    graphics.ClipBounds.X + LinearPlotSettings.MarginLeft,
                    graphics.ClipBounds.Y + LinearPlotSettings.MarginTop,
                    graphics.ClipBounds.Width - LinearPlotSettings.MarginLeft - LinearPlotSettings.MarginRight,
                    graphics.ClipBounds.Height - LinearPlotSettings.MarginTop - LinearPlotSettings.MarginBottom
                ));

                drawPlot(graphics, minX, maxX, minY, maxY);
            }
        }

        public void Redraw()
        {
            pictureBox.Invalidate();
        }


        public LinearPlot()
        {
            InitializeComponent();
            pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            // EventHandler redrawCallback = (object sender, EventArgs args) => { draw(); 
            pictureBox.Paint += (object sender, PaintEventArgs args) => { draw(args.Graphics); };
        }



    }
}
