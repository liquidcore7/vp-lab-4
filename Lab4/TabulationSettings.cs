﻿using System.Collections.Generic;

namespace Lab4
{
    public struct TabulationSettings
    {
        public readonly double LowerBound;
        public readonly double UpperBound;
        public readonly double Step;

        public static readonly TabulationSettings Default = new TabulationSettings(
            -10.0,
             10.0,
             0.1
        );

        public TabulationSettings(double lower, double upper, double step)
        {
            this.LowerBound = lower;
            this.UpperBound = upper;
            this.Step = step;
        }

        public IEnumerable<double> PointsStream()
        {
            for (double x = LowerBound; x <= UpperBound; x += Step) yield return x;
        }
    }
}
