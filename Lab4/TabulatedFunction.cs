﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Lab4
{
    public class TabulatedFunction
    {
        private readonly Func<double, double> f;
        public readonly string ReadableName;

        public TabulatedFunction(string name, Func<double, double> func)
        {
            this.f = func;
            this.ReadableName = name;
        }


        public IEnumerable<Point> Tabulate(TabulationSettings settings)
        {
            return settings
                .PointsStream()
                .Select(x => new Point(x, f(x)));
        }

        public TabulatedFunction Compose(TabulatedFunction another)
        {
            return new TabulatedFunction(another.ReadableName + " ∘ " + ReadableName, x => another.f(f(x)));
        }

    }
}
