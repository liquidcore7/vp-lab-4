﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Lab4
{
    public partial class FunctionPlotter : UserControl
    {
        private TabulationSettings tabulationSettings = TabulationSettings.Default;
        private TabulatedFunction function;

        private void updateData()  // TODO: use single collection for both plots
        {
            chart.Points.Clear();
            dataGrid.Rows.Clear();

            var points = function.Tabulate(tabulationSettings);
            var ys = points.Select(p => p.Y);

            foreach (Point p in points)
            {
                dataGrid.Rows.Add(p.X, p.Y);
                chart.Points.Add(p);
            }

            chart.Redraw();

            chartSettings.SetMin(ys.Min());
            chartSettings.SetMax(ys.Max());
        }

        public FunctionPlotter()
        {
            InitializeComponent();

            this.chartSettings.Apply(tabulationSettings);
            this.chartSettings.RegisterUpdateHandler((object sender, EventArgs args) =>
            {
                this.tabulationSettings = this.chartSettings.GetTabulationSettings();
                updateData();
            });

            dataGrid.Columns.Add("X", "X");
            dataGrid.Columns.Add("Y", "Y");

        }

        public void SetFunction(TabulatedFunction function)
        {
            this.function = function;

            chart.FunctionName = function.ReadableName;
            updateData();
        }
    }
}
