﻿namespace Lab4
{
    partial class NumericInput
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layout = new System.Windows.Forms.TableLayoutPanel();
            this.label = new System.Windows.Forms.Label();
            this.valuePicker = new System.Windows.Forms.NumericUpDown();
            this.layout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.valuePicker)).BeginInit();
            this.SuspendLayout();
            // 
            // layout
            // 
            this.layout.ColumnCount = 2;
            this.layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.07107F));
            this.layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.92893F));
            this.layout.Controls.Add(this.label, 0, 0);
            this.layout.Controls.Add(this.valuePicker, 1, 0);
            this.layout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout.Location = new System.Drawing.Point(0, 0);
            this.layout.Name = "layout";
            this.layout.RowCount = 1;
            this.layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layout.Size = new System.Drawing.Size(197, 39);
            this.layout.TabIndex = 0;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label.Location = new System.Drawing.Point(3, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(69, 39);
            this.label.TabIndex = 0;
            this.label.Text = "label1";
            this.label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // valuePicker
            // 
            this.valuePicker.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.valuePicker.DecimalPlaces = 2;
            this.valuePicker.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.valuePicker.Location = new System.Drawing.Point(78, 8);
            this.valuePicker.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.valuePicker.Name = "valuePicker";
            this.valuePicker.Size = new System.Drawing.Size(116, 22);
            this.valuePicker.TabIndex = 1;
            // 
            // NumericInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layout);
            this.Name = "NumericInput";
            this.Size = new System.Drawing.Size(197, 39);
            this.layout.ResumeLayout(false);
            this.layout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.valuePicker)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel layout;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.NumericUpDown valuePicker;
    }
}
