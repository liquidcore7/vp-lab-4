﻿using System;
using System.Windows.Forms;

namespace Lab4
{
    public partial class ChartSettings : UserControl
    {
        public ChartSettings()
        {
            InitializeComponent();
            lowerBound.SetLabel("From");
            upperBound.SetLabel("To");
            step.SetLabel("Step");
        }

        public void SetMin(double min)
        {
            this.minValue.Text = min.ToString();
        }
        public void SetMax(double max)
        {
            this.maxValue.Text = max.ToString();
        }

        public void RegisterUpdateHandler(EventHandler handler)
        {
            foreach (NumericInput input in new NumericInput[] {this.lowerBound, this.upperBound, this.step})
            {
                input.RegisterUpdateHandler(handler);
            }
        }

        public void Apply(TabulationSettings settings)
        {
            this.lowerBound.SetMaxValue(settings.UpperBound);
            this.upperBound.SetMinValue(settings.LowerBound);

            this.lowerBound.SetValue(settings.LowerBound);
            this.upperBound.SetValue(settings.UpperBound);
            this.step.SetValue(settings.Step);
            this.step.SetMinValue(0.05);
        }


        public TabulationSettings GetTabulationSettings()
        {
            return new TabulationSettings(
                this.lowerBound.GetValue(),
                this.upperBound.GetValue(),
                this.step.GetValue()
            );
        }

        
    }
}
